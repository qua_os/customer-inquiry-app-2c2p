namespace CustomerInquirySvc
{
    public class AppConstants
    {
        public static class ConfigSections {
            public const string APP = "CustomerInquiryApp";
            public const string APP_CLIENTS = "Clients";
            public const string ELEM_APP_CLIENTS_CLIENT = "Client";
            public const string ATTR_APP_CLIENTS_CLIENT_ID = "id";
            public const string ATTR_APP_CLIENTS_CLIENT_TOKEN = "token";

            public const string APP_SVC = "Service";
            public const string ATTR_APP_SVC_MAX_CUST_RECENT_TRANSACTIONS = "maxCustomerRecentTransactions";
        }
        
        public static class RequestHeaders {
            public const string CLIENT_ID = "X-App-Client-ID";
            public const string CLIENT_TOKEN = "X-App-Client-Token";
        }
    }
}