using System.Collections.Generic;
using CustomerInquirySvc.Models;
using CustomerInquirySvc.Utilities;
using Newtonsoft.Json;

namespace CustomerInquirySvc.ViewModels
{
    public class CustomerViewModel
    {
        public CustomerViewModel() {
            this.transactions = new List<CustomerTransactionViewModel>();
        }
        public CustomerViewModel(Customer src) : this() {
            this.SourceModel = src;
            if (src != null) {
                //ObjectUtils.copy(src, this);
                this.customerID = src.CustomerId;
                this.name = src.CustomerName;
                this.email = src.ContactEmail;
                this.mobile = src.MobileNo;
            }
        }

        [JsonIgnore]
        public Customer SourceModel { get; protected set; }

        public string customerID { get; set; }

        public string name { get; set; }

        public string email { get; set; }

        public string mobile { get; set; }

        public ICollection<CustomerTransactionViewModel> transactions { get; set; }

    }
}