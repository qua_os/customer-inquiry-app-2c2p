using System;
using System.Collections.Generic;
using CustomerInquirySvc.Models;
using CustomerInquirySvc.Utilities;
using Newtonsoft.Json;

namespace CustomerInquirySvc.ViewModels
{
    public class CustomerTransactionViewModel
    {
        
        public CustomerTransactionViewModel() {
        }
        public CustomerTransactionViewModel(CustomerTransaction src) : this() {
            this.SourceModel = src;
            if (src != null) {
                //ObjectUtils.copy(src, this);
                this.id = src.TransactionIdAsString;
                this.customerID = src.CustomerId;
                this.date = src.TransactionDateTime;
                this.amount = src.AmountAsDouble;
                this.currency = src.CurrencyCode;
                this.status = src.Status;
            }
        }

        [JsonIgnore]
        public CustomerTransaction SourceModel { get; protected set; }

        public string id { get; set; }
        public string customerID { get; set; }
        public DateTime date { get; set; }
        public double amount { get; set; }
        public string currency { get; set; }
        public string status { get; set; }

    }
}