﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerInquirySvc.Filters;
using CustomerInquirySvc.Models;
using CustomerInquirySvc.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CustomerInquirySvc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiExceptionTranslator]
    public class CurrenciesController : BaseControllerImpl
    {
        protected ICurrenciesService CurrenciesService { get; set; }

        public CurrenciesController(IConfiguration appConfig,
            CustomerInquiryDbContext dbContext,
            IHostingEnvironment env,
            ICurrenciesService currenciesService)
            : base(appConfig, dbContext, env)
        {
            this.CurrenciesService = currenciesService;
        }
        
        [HttpGet]
        public ActionResult<ICollection<Currency>> GetCurrencies([FromQuery]string orderBy, [FromQuery]bool? orderDesc)
        {
            QueryOptions qOpts = GetQueryOptions(orderBy: orderBy, orderDesc: orderDesc);

            IEnumerable<Currency> Currencies = CurrenciesService.GetCurrencies(qOpts);

            return Currencies.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Currency> GetCurrency([FromRoute]string id)
        {
            QueryOptions qOpts = GetQueryOptions();

            return CurrenciesService.GetCurrency(id, qOpts);
        }

        /*
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        */
    }
}
