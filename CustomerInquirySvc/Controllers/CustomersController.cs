﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerInquirySvc.Filters;
using CustomerInquirySvc.Models;
using CustomerInquirySvc.Services;
using CustomerInquirySvc.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace CustomerInquirySvc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiExceptionTranslator]
    public class CustomersController : BaseControllerImpl
    {
        protected AppSettings AppSettings;
        protected AppSettings.ServiceSettings ServiceSettings;
        protected ICustomersService CustomersService { get; set; }
        protected ICustomerTransactionsService CustomerTransactionsService { get; set; }

        public CustomersController(IConfiguration appConfig,
            CustomerInquiryDbContext dbContext,
            IHostingEnvironment env,
            IOptions<AppSettings> appSettingsOpts,
            IOptions<AppSettings.ServiceSettings> svcSettingsOpts,
            ICustomersService custService,
            ICustomerTransactionsService transService)
            : base(appConfig, dbContext, env)
        {
            this.AppSettings = (appSettingsOpts != null) ? appSettingsOpts.Value : null;
            //TODO: Revise this later (Workaround for sub-section options not populated)
            var svcSettings = (this.AppSettings != null) ? AppSettings.Service : null;
            /*var svcSettings = (svcSettingsOpts != null) ? svcSettingsOpts.Value : null;
            if (svcSettings == null) {
                svcSettings = (this.AppSettings != null) ? AppSettings.Service : null;
            }*/
            this.ServiceSettings = svcSettings;
            this.CustomersService = custService;
            this.CustomerTransactionsService = transService;
        }
        
        [HttpGet]
        public ActionResult<object> GetCustomers([FromQuery]CustomerQueryFilters filters,
            [FromQuery]string orderBy, [FromQuery]bool? orderDesc)
        {
            CheckValidateApiModel(filters, new Func<CustomerQueryFilters, bool>[] {
                (m) => {
                    if ((m == null)
                        || ((String.IsNullOrEmpty(m.customerID))
                            && (String.IsNullOrEmpty(m.email))))
                    {
                        throw new Exception("No inquiry criteria");
                    }

                    return true;
                }
            });

            QueryOptions qOpts = GetQueryOptions(orderBy: orderBy, orderDesc: orderDesc);
            int? confMaxRecentTrans = getMaxRecentTransactionsFromConfig();
            int? maxRecentTrans = (filters != null) ? filters.maxRecentTransactions : null;
            if ((maxRecentTrans == null)
                || ((confMaxRecentTrans != null) && (maxRecentTrans.Value > confMaxRecentTrans.Value)))
            {
                maxRecentTrans = confMaxRecentTrans;
            }
            maxRecentTrans = maxRecentTrans ?? 0; //Int32.MaxValue;

            var customerVMs = CustomersService.GetCustomersAs<CustomerViewModel>(filters,
                //Using ViewModel as columns select DTO
                (cust) => new CustomerViewModel() {
                    customerID = cust.CustomerId,
                    name = cust.CustomerName,
                    email = cust.ContactEmail,
                    transactions = cust.Transactions
                        .OrderByDescending(tr => tr.TransactionDateTime)
                        .Take(maxRecentTrans.Value)
                        .Select(e => new CustomerTransactionViewModel() {
                            id = e.TransactionIdAsString,
                            customerID = e.CustomerId,
                            date = e.TransactionDateTime,
                            amount = e.AmountAsDouble,
                            currency = e.CurrencyCode,
                            status = e.Status
                        })
                        .ToList()
                },
                qOpts);
            if ((customerVMs == null) || (customerVMs.Count() <= 0)) {
                if ((!String.IsNullOrEmpty(filters.customerID))) {
                    throw new ApiException(404, "Invalid Customer ID");
                } else if ((!String.IsNullOrEmpty(filters.email))) {
                    throw new ApiException(404, "Invalid Email");
                }
                throw new ApiException(404, "Customer Not Found");
            }
            if (customerVMs.Count() == 1) {
                return customerVMs.First();
            }

            return customerVMs.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<CustomerViewModel> GetCustomer([FromRoute]string id,
            [FromQuery]int? maxRecentTransactions)
        {
            QueryOptions qOpts = GetQueryOptions();
            int? confMaxRecentTrans = getMaxRecentTransactionsFromConfig();
            if ((maxRecentTransactions == null)
                || ((confMaxRecentTrans != null) && (maxRecentTransactions.Value > confMaxRecentTrans.Value)))
            {
                maxRecentTransactions = confMaxRecentTrans;
            }

            var customerVM = CustomersService.GetCustomerAs<CustomerViewModel>(id,
                //Using ViewModel as columns select DTO
                (cust) => new CustomerViewModel() {
                    customerID = cust.CustomerId,
                    name = cust.CustomerName,
                    email = cust.ContactEmail,
                    transactions = cust.Transactions
                        .OrderByDescending(tr => tr.TransactionDateTime)
                        .Take(maxRecentTransactions.Value)
                        .Select(e => new CustomerTransactionViewModel() {
                            id = e.TransactionIdAsString,
                            customerID = e.CustomerId,
                            date = e.TransactionDateTime,
                            amount = e.AmountAsDouble,
                            currency = e.CurrencyCode,
                            status = e.Status
                        })
                        .ToList()
                },
                qOpts);
            if (customerVM == null) {
                throw new ApiException(404, "Invalid Customer ID");
            }

            return customerVM;
        }

        /*
        // POST api/values
        [HttpPost]
        public void Post([FromBody]Customer cust)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put([FromRoute]string id, [FromBody]Customer cust)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete([FromRoute]string id)
        {
        }
        */

        protected int? getMaxRecentTransactionsFromConfig() {
            var svcSection = ServiceSettings; //AppConfig.GetSection(AppConstants.ConfigSections.APP_SVC);
            if (svcSection == null) {
                return null;
            }

            var maxTrans = svcSection.maxCustomerRecentTransactions;
            /* svcSection.GetValue<string>(AppConstants.ConfigSections
                .ATTR_APP_SVC_MAX_CUST_RECENT_TRANSACTIONS);
            if (String.IsNullOrWhiteSpace(maxTransStr)) {
                return null;
            }
            */

            return maxTrans;
        }
    }
}
