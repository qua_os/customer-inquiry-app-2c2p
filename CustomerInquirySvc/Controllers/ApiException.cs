using System;

namespace CustomerInquirySvc.Controllers
{
    public class ApiException : Exception
    {
        public ApiException(string msg) : base(msg) {
        }
        public ApiException(int statusCode, string msg) : this(msg) {
            this.statusCode = statusCode;
        }
        
        public ApiException(string msg, Exception innerEx) : base(msg, innerEx) {
        }
        public ApiException(int statusCode, string msg, Exception innerEx) : this(msg, innerEx) {
            this.statusCode = statusCode;
        }

        public int? statusCode { get; set; }
    }
}