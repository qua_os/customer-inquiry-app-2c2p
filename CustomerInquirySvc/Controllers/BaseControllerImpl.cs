using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CustomerInquirySvc.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
//using Newtonsoft.Json;

namespace CustomerInquirySvc.Controllers
{
    public class BaseControllerImpl : ControllerBase
    {
        protected readonly IConfiguration AppConfig;
        protected readonly CustomerInquiryDbContext DbContext;
        //protected readonly JsonSerializerSettings JsonSettings;
        protected readonly IHostingEnvironment Env;

        public BaseControllerImpl(IConfiguration appConfig,
            CustomerInquiryDbContext db,
            IHostingEnvironment env)
            : base()
        {
            this.AppConfig = appConfig;
            this.DbContext = db;
            this.Env = env;
        }

        public QueryOptions GetQueryOptions(IEnumerable<string> columns = null,
            string orderBy = null, bool? orderDesc = null)
        {
            return new QueryOptions() {
                Db = this.DbContext,
                User = User,
                SelectedColumns = columns,
                OrderBy = orderBy,
                OrderDesc = orderDesc
            };
        }

        public void CheckValidateApiModel<T>(T model, IEnumerable<Func<T, bool>> auxValidateFns = null) {
            if (!ModelState.IsValid) {
                var mErrors = ModelState.Where(e => e.Value.Errors.Count >= 1);
                var sb = new StringBuilder();
                sb.Append("Field Validation Error for fields: ");
                int i = 0;
                foreach (var mError in mErrors) {
                    if (i > 0) {
                        sb.Append(", ");
                    }
                    sb.Append(mError.Key);
                }

                throw new ApiException(400, sb.ToString());
            }
            if ((auxValidateFns != null) && (auxValidateFns.Count() >= 1)) {
                var sb = new StringBuilder();
                int errorsCount = 0;
                foreach (var validateFn in auxValidateFns) {
                    try {
                        if (!validateFn.Invoke(model)) {
                            throw new Exception("Invalid Data");
                        }
                    } catch (Exception ex) {
                        errorsCount++;
                        sb.Append(ex.Message);
                        sb.Append("\n");
                    }
                }
                if (errorsCount > 0) {
                    throw new ApiException(400, sb.ToString());
                }
            }
        }
    }
}