using CustomerInquirySvc.Controllers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace CustomerInquirySvc.Filters
{
    public class ApiExceptionFilter : IExceptionFilter
    {
        protected readonly IHostingEnvironment Env;
        //protected IModelMetadataProvider MetadataProvider;

        public ApiExceptionFilter(IHostingEnvironment hostingEnvironment)
        {
            Env = hostingEnvironment;
            //MetadataProvider = modelMetadataProvider;
        }
        
        public void OnException(ExceptionContext context) {
            var ex = context.Exception;
            if (!(ex is ApiException)) {
                return;
            }
            int? statusCode = ((ApiException)ex).statusCode;
            JsonResult result = (Env.IsDevelopment())
                ? new JsonResult(new {
                    ErrorMessage = ex.Message,
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    InnerException = ex.InnerException
                })
                : new JsonResult(new {
                    ErrorMessage = ex.Message
                });
            result.StatusCode = statusCode ?? 500;
            context.Result = result;
            context.ExceptionHandled = true;
        }
    }
}