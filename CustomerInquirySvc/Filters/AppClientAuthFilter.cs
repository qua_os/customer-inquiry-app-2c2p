using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CustomerInquirySvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace CustomerInquirySvc.Filters
{
    public class AppClientAuthFilter : IAuthorizationFilter
    {
        protected readonly IConfiguration AppConfig;
        //TEST:
        protected readonly AppSettings AppSettings;
        protected readonly AppSettings.ClientSettingsCollection ClientsSettings;
        protected readonly Dictionary<string, AppSettings.ClientSettings> ClientSettingsMap;

        public AppClientAuthFilter(IConfiguration appConfig,
            IOptions<AppSettings> appSettingsOpts,
            IOptions<AppSettings.ClientSettingsCollection> clientsSettingsOpts)
        {
            this.AppConfig = appConfig;
            this.AppSettings = (appSettingsOpts != null) ? appSettingsOpts.Value : null;
            //TODO: Revise this later (Workaround for sub-section options not populated)
            var clientsSettings = (this.AppSettings != null) ? this.AppSettings.Clients : null;
            /*
            var clientsSettings = (clientsSettingsOpts != null)
                ? clientsSettingsOpts.Value
                : null;
            if (((clientsSettings == null) || (clientsSettings.Count <= 0))
                && (this.AppSettings != null))
            {
                clientsSettings = this.AppSettings.Clients;
            }
            */
            this.ClientsSettings = clientsSettings;
            this.ClientSettingsMap = (clientsSettings != null)
                ? clientsSettings.ToDictionary(e => e.id)
                : new Dictionary<string, AppSettings.ClientSettings>();
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var req = context.HttpContext.Request;

            string clientID = (req.Headers.ContainsKey(AppConstants.RequestHeaders.CLIENT_ID))
                ? req.Headers[AppConstants.RequestHeaders.CLIENT_ID].ToString()
                : null;
            string clientToken = (req.Headers.ContainsKey(AppConstants.RequestHeaders.CLIENT_TOKEN))
                ? req.Headers[AppConstants.RequestHeaders.CLIENT_TOKEN].ToString()
                : null;
                
            Debug.WriteLine("Got client auth: {0}:{1}", (object)clientID, (object)clientToken);
            //TODO: Check client ID/Token with app config
            if (String.IsNullOrEmpty(clientID)) {
                //throw new ApiException(401, "Invalid Client ID/Token");
                return;
            }
            var clientSettings = (ClientSettingsMap.ContainsKey(clientID))
                ? ClientSettingsMap[clientID]
                : null;
            if (clientSettings == null) {
                //throw new ApiException(401, "Invalid Client ID/Token");
                return;
            }
            if ((clientID != clientSettings.id) || (clientToken != clientSettings.token)) {
                //throw new ApiException(401, "Invalid Client ID/Token");
                return;
            }
        }
    }
}