using CustomerInquirySvc.Controllers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace CustomerInquirySvc.Filters
{
    public class ApiExceptionTranslatorAttribute : ExceptionFilterAttribute
    {
        //protected readonly IHostingEnvironment Env;
        //protected IModelMetadataProvider MetadataProvider;

        public ApiExceptionTranslatorAttribute() : base() {
        }
        
        public override void OnException(ExceptionContext context)
        {
            var ex = context.Exception;
            if (ex is ApiException) {
                return;
            }
            /*
            if (ex is DbEntityException) {
                context.Exception =
            }
            */
            context.Exception = new ApiException(ex.Message, ex);
        }
    }
}