﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerInquirySvc.Filters;
using CustomerInquirySvc.Models;
using CustomerInquirySvc.Services;
using CustomerInquirySvc.Services.Impl;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Binder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CustomerInquirySvc
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var appSettingsSection = 
                Configuration.GetSection(AppConstants.ConfigSections.APP);
            //TEST
            //var svcSection = appSettingsSection.Get<AppSettings.ServiceSettings>();
            services.AddOptions();
            services.Configure<AppSettings>((options) => appSettingsSection.Bind(options));

            /*services.AddAuthorization(options => {
                options.AddPolicy()
            });*/
            services.AddMvc(options => {
                options.Filters.Add(typeof(AppClientAuthFilter));
                //options.Filters.Add(typeof(AuthorizeFilter));
                options.Filters.Add(typeof(ApiExceptionFilter));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddDbContext<CustomerInquiryDbContext>(options => {
                options.UseSqlServer(
                    Configuration.GetConnectionString("CustomerInquiryDB"),
                        e => e.MigrationsAssembly("CustomerInquirySvc"));
            });
        
            //TODO:
            //services.AddSingleton<IAppAuthService, AppAuthServiceImpl>();
            //services.AddSingleton<IUserAuthService, UserAuthServiceImpl>();
            services.AddSingleton<ICustomersService, CustomersServiceImpl>();
            services.AddSingleton<ICustomerTransactionsService, CustomerTransactionsServiceImpl>();
            services.AddSingleton<ICurrenciesService, CurrenciesServiceImpl>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseStaticFiles();
        }
    }
}
