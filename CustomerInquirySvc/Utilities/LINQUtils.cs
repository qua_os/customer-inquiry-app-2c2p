using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CustomerInquirySvc.Utilities
{
    public class LINQUtils
    {
        protected static Type IENUM_TYPE = typeof(IEnumerable<>);
        protected static Type STRING_TYPE = typeof(String);
        protected static Type DECIMAL_TYPE = typeof(decimal);
        protected static Type DATETIME_TYPE = typeof(DateTime);

        protected static Func<DbContext, object, EntityEntry> FIX_STATE_FN = (db, item) => {
            var dbEntry = db.Entry(item);
            if (dbEntry.State != EntityState.Detached) {
                dbEntry.State = EntityState.Unchanged;
            }

            return dbEntry;
        };

        public static IQueryable<T> ApplyFilters<T, TFilters>(IQueryable<T> q,
            TFilters filters,
            Dictionary<string, Func<IQueryable<T>, object, IQueryable<T>>> filterFnsMap = null)
        {
            Type fType = typeof(TFilters);
            
            if ((filters != null) && (filterFnsMap != null)) {
                var fProps = fType.GetProperties();
                foreach (var fProp in fProps) {
                    if (fProp.IsSpecialName) {
                        continue;
                    }
                    var filterFn = (filterFnsMap.ContainsKey(fProp.Name))
                        ? filterFnsMap[fProp.Name]
                        : null;
                    if (filterFn != null) {
                        var fVal = fProp.GetValue(filters);
                        q = filterFn.Invoke(q, fVal);
                    }
                }
            }

            return q;
        }

        public static IQueryable<T> ApplyOrdering<T>(IQueryable<T> q, 
            string orderBy = null, bool orderDesc = false,
            Dictionary<string, Func<IQueryable<T>, bool, IQueryable<T>>> orderFnsMap = null)
        {
            Type mType = typeof(T);

            if (!String.IsNullOrEmpty(orderBy)) {
                var orderFn = ((orderFnsMap != null) && (orderFnsMap.ContainsKey(orderBy)))
                    ? orderFnsMap[orderBy]
                    : null;
                if (orderFn != null) {
                    q = orderFn.Invoke(q, orderDesc);
                } else {
                    var orderProp = mType.GetProperty(orderBy);
                    q = (orderDesc)
                        ? q.OrderByDescending(e => orderProp.GetValue(e))
                        : q.OrderBy(e => orderProp.GetValue(e));
                }
            }

            return q;
        }

        public static void FixUpdateRelatedEntityStates<T>(DbContext db, T item) where T : class {
            Type mType = typeof(T);
            var props = mType.GetProperties();

            foreach (var prop in props) {
                Type pType = prop.PropertyType;
                var pVal = prop.GetValue(item);
                if (pVal == null) {
                    continue;
                }
                if (isIEnumerable(pType)) {
                    Type itemType = pType.GenericTypeArguments[0];
                    if (!isSimpleType(itemType)) {
                        continue;
                    }
                    int i = 0;
                    foreach (var pItem in (IEnumerable)pVal) {
                        if (pItem == null) {
                            continue;
                        }
                        Debug.WriteLine("Fixing Entity State for: {0}.{1}[{2}]", mType.Name, pType.Name, i);
                        FIX_STATE_FN.Invoke(db, pItem);
                        i++;
                    }
                } else {
                    if (!isSimpleType(pType)) {
                        continue;
                    }
                    Debug.WriteLine("Fixing Entity State for: {0}.{1}[{2}]", mType.Name, pType.Name);
                    FIX_STATE_FN.Invoke(db, pVal);
                }
            }
        }
        protected static IEnumerable CastGenericEnum(IEnumerable src, Type itemType) {
            var castMethod = typeof(Enumerable).GetMethod("Cast", BindingFlags.Static | BindingFlags.Public);
            var genCastMethod = castMethod.MakeGenericMethod(new Type[] { itemType });

            return (IEnumerable)genCastMethod.Invoke(src, new object[] { });
        }
        protected static bool isIEnumerable(Type type) {
            return (type.GetInterfaces()
                .Any(t => (t.IsGenericType) && (t.GetGenericTypeDefinition() == IENUM_TYPE)))
                && (!STRING_TYPE.IsAssignableFrom(type));
        }
        protected static bool isSimpleType(Type type) {
            return (STRING_TYPE.IsAssignableFrom(type))
                || (DATETIME_TYPE.IsAssignableFrom(type))
                || (DECIMAL_TYPE.IsAssignableFrom(type))
                || (type.IsPrimitive);

        }
    }
}