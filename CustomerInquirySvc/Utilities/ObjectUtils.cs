using System;
using System.Reflection;

namespace CustomerInquirySvc.Utilities
{
    public class ObjectUtils
    {
        protected static Type ACTIVATOR_TYPE = typeof(Activator);
        protected static MethodInfo ACTIVATOR_CREATE_T_METHOD = ACTIVATOR_TYPE.GetMethod("CreateInstance<>");

        public static T2 copy<T1, T2>(T1 src, T2 dest = default(T2))
            where T1 : class
            where T2 : class
        {
            if (dest == null) {
                dest = Activator.CreateInstance<T2>();
            }

            copyFieldsInternal(src, typeof(T1), dest, typeof(T2));

            return dest;
        }
        public static object copy(object src, object dest = null) {
            Type srcType = src.GetType();
            Type destType;
            if (dest != null) {
                destType = dest.GetType();
            } else {
                destType = srcType;
                dest = ACTIVATOR_CREATE_T_METHOD.MakeGenericMethod(destType)
                    .Invoke(null, new object[] { });
            }

            copyFieldsInternal(src, srcType, dest, destType);

            return dest;
        }
        protected static void copyFieldsInternal(object src, Type srcType, object dest, Type destType) {
            foreach (var prop1 in srcType.GetProperties()) {
                Type pType1 = prop1.PropertyType;
                PropertyInfo prop2 = destType.GetProperty(prop1.Name);
                if (prop2 == null) {
                    continue;
                }
                Type pType2 = prop2.PropertyType;
                if (!pType2.IsAssignableFrom(pType1)) {
                    continue;
                }
                prop2.SetValue(dest, prop1.GetValue(src));
            }
        }
    }
}