
using System.Collections.Generic;

namespace CustomerInquirySvc {
    public class AppSettings {
        public string version { get; set; }
        public ClientSettingsCollection Clients { get; set; }
        public ServiceSettings Service { get; set; }

        public class ClientSettingsCollection : List<ClientSettings> {
        }

        public class ClientSettings {
            public string id { get; set; }
            public string token { get; set; }
        }
        
        public class ServiceSettings {
            public string certificateName { get; set; }
            public int? maxCustomerRecentTransactions { get; set; }
        }
    }

}
