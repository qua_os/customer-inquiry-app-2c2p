﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerInquirySvc.Services
{
    public interface IAppAuthService
    {
        bool authenticate(string appID, string appToken, string appPublicKey = null);
    }
}
