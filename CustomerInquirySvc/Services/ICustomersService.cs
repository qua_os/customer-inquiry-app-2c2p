﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerInquirySvc.Models;

namespace CustomerInquirySvc.Services
{
    public interface ICustomersService
    {
        IEnumerable<Customer> GetCustomers(CustomerQueryFilters filters, QueryOptions qOpts);
        IEnumerable<T> GetCustomersAs<T>(CustomerQueryFilters filters, Func<Customer, T> transformFn,
            QueryOptions qOpts) where T : class;
        Customer GetCustomer(string id, /*int? maxRecentTransactions,*/ QueryOptions qOpts);
        T GetCustomerAs<T>(string id, /*int? maxRecentTransactions,*/ Func<Customer, T> transformFn,
            QueryOptions qOpts) where T : class;
        bool AddCustomer(Customer cust, QueryOptions qOpts);
        bool UpdateCustomer(Customer cust, QueryOptions qOpts);
        bool DeleteCustomer(Customer cust, QueryOptions qOpts);
    }
}
