﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerInquirySvc.Models;

namespace CustomerInquirySvc.Services
{
    public interface ICurrenciesService
    {
        IEnumerable<Currency> GetCurrencies(QueryOptions qOpts);
        Currency GetCurrency(string id, QueryOptions qOpts);
        bool AddCurrency(Currency currency, QueryOptions qOpts);
        bool UpdateCurrency(Currency currency, QueryOptions qOpts);
        bool DeleteCurrency(Currency currency, QueryOptions qOpts);
    }
}
