﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerInquirySvc.Models;

namespace CustomerInquirySvc.Services
{
    public interface ICustomerTransactionsService
    {
        IEnumerable<CustomerTransaction> GetCustomerTransactions(CustomerTransactionQueryFilters filters, QueryOptions qOpts);
        CustomerTransaction GetCustomerTransaction(long id, QueryOptions qOpts);
        bool AddCustomerTransaction(CustomerTransaction transaction, QueryOptions qOpts);
        bool UpdateCustomerTransaction(CustomerTransaction transaction, QueryOptions qOpts);
        bool DeleteCustomerTransaction(CustomerTransaction transaction, QueryOptions qOpts);
    }
}
