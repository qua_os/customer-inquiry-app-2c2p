﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerInquirySvc.Services
{
    public interface IUserAuthService
    {
        bool authenticate(string userName, string password, string domain = null, string appPublicKey = null);
    }
}
