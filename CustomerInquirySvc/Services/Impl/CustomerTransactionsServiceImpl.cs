using System;
using System.Collections.Generic;
using System.Linq;
using CustomerInquirySvc.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CustomerInquirySvc.Services.Impl
{
    public class CustomerTransactionsServiceImpl : BaseServiceImpl, ICustomerTransactionsService
    {
        public CustomerTransactionsServiceImpl(IConfiguration appConfig, ILoggerFactory loggerFactory)
            : base(appConfig, loggerFactory)
        {
        }
        
        public IEnumerable<CustomerTransaction> GetCustomerTransactions(CustomerTransactionQueryFilters filters,
            QueryOptions qOpts)
        {
            return GetCollection<CustomerTransaction, CustomerTransactionQueryFilters, CustomerTransaction>(qOpts.Db, filters: filters,
                orderBy: qOpts.OrderBy, orderDesc: qOpts.OrderDesc ?? false,
                postSelectFn: (q) => {
                    q = q.Include(e => e.Customer)
                        .Include(e => e.Currency);
                    if (filters != null) {
                        if (!String.IsNullOrEmpty(filters.CustomerId)) {
                            q = q.Where(e => (e.CustomerId == filters.CustomerId));
                        }
                        if (filters.SinceDateTime != null) {
                            q = q.Where(e => (e.TransactionDateTime >= filters.SinceDateTime.Value));
                        }
                        if (filters.BeforeDateTime != null) {
                            q = q.Where(e => (e.TransactionDateTime < filters.BeforeDateTime.Value));
                        }
                        if (!String.IsNullOrEmpty(filters.Status)) {
                            q = q.Where(e => (e.Status == filters.Status));
                        }
                    }

                    return q;
                }
            ) ;
        }
        public CustomerTransaction GetCustomerTransaction(long id, QueryOptions qOpts) {
            return GetItem<CustomerTransaction, long, CustomerTransaction>(qOpts.Db, key: id,
                orderBy: qOpts.OrderBy, orderDesc: qOpts.OrderDesc ?? false,
                postSelectFn: (q) => q.Include(e => e.Customer)
                    .Include(e => e.Currency)
                    .Where(e => (e.TransactionId == id))
            );
        }

        public bool AddCustomerTransaction(CustomerTransaction cust, QueryOptions qOpts) {
            throw new Exception("Not implemented yet");
        }
        
        public bool UpdateCustomerTransaction(CustomerTransaction cust, QueryOptions qOpts) {   
            throw new Exception("Not implemented yet");
        }
        
        public bool DeleteCustomerTransaction(CustomerTransaction cust, QueryOptions qOpts) {
            throw new Exception("Not implemented yet");
        }
    }
}