using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CustomerInquirySvc.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CustomerInquirySvc.Services.Impl
{
    /**
     * Base CRUD Service Implementation Template
     */
    public class BaseServiceImpl
    {
        protected IConfiguration AppConfig;
        protected ILoggerFactory LoggerFactory;

        public BaseServiceImpl(IConfiguration appConfig, ILoggerFactory loggerFactory) {
            this.AppConfig = appConfig;
            this.LoggerFactory = loggerFactory;
        }
        
        public IEnumerable<TOut> GetCollection<T, TFilters, TOut>(DbContext db, TFilters filters = default(TFilters),
            string orderBy = null, bool orderDesc = false,
            Func<IQueryable<T>, IQueryable<T>> postSelectFn = null,
            Dictionary<string, Func<IQueryable<T>, object, IQueryable<T>>> filterFnsMap = null,
            Func<IQueryable<T>, IQueryable<T>> postFilterFn = null,
            Dictionary<string, Func<IQueryable<T>, bool, IQueryable<T>>> orderFnsMap = null,
            Func<IQueryable<T>, IQueryable<T>> postOrderFn = null,
            Func<T, TOut> transformFn = null,
            Func<IEnumerable<TOut>, IEnumerable<TOut>> postQueryFn = null,
            ILogger logger = null)
            where T : class
            where TOut : class
        {
            Type mType = typeof(T);
            Type fType = typeof(TFilters);

            /*if (logger == null) {
                logger = this.Logger;
            }*/
            //db.Database.Logger = Logger;
            var q = db.Set<T>().AsQueryable();
            if (postSelectFn != null) {
                q = postSelectFn.Invoke(q);
            }
            if (q == null) {
                return null;
            }

            if (filters != null) {
                q = LINQUtils.ApplyFilters(q, filters, filterFnsMap);
            }
            if (q == null) {
                return null;
            }
            if (postFilterFn != null) {
                q = postFilterFn.Invoke(q);
            }
            if (q == null) {
                return null;
            }
            
            q = LINQUtils.ApplyOrdering(q, orderBy, orderDesc, orderFnsMap);
            if (postOrderFn != null) {
                q = postOrderFn.Invoke(q);
            }
            if (q == null) {
                return null;
            }

            //TODO: Revise this later
            IQueryable<TOut> q2 = (transformFn != null)
                ? q.Select(e => transformFn.Invoke(e))
                : ((typeof(T) == typeof(TOut))
                    ? (IQueryable<TOut>)q
                    : null);
            if (q2 == null) {
                return null;
            }

            IEnumerable<TOut> list = q2.ToList();
            if (postQueryFn != null) {
                var list2 = postQueryFn.Invoke(list);
                if (list2 != null) {
                    list = list2;
                }
            }

            return list;
        }
        public TOut GetItem<T, TKey, TOut>(DbContext db, TKey key,
            string orderBy = null, bool orderDesc = false,
            Func<IQueryable<T>, IQueryable<T>> postSelectFn = null,
            Func<IQueryable<T>, TKey, IQueryable<T>> findItemFn = null,
            Dictionary<string, Func<IQueryable<T>, bool, IQueryable<T>>> orderFnsMap = null,
            Func<IQueryable<T>, IQueryable<T>> postOrderFn = null,
            Func<T, TOut> transformFn = null,
            Func<TOut, TOut> postQueryFn = null)
            where T : class
            where TOut : class
        {
            Type mType = typeof(T);
            Type kType = typeof(TKey);

            if (key == null) {
                return null;
            }

            var q = db.Set<T>().AsQueryable();
            if (postSelectFn != null) {
                q = postSelectFn.Invoke(q);
            }
            if (q == null) {
                return null;
            }

            if (findItemFn != null) {
                q = findItemFn.Invoke(q, key);
            }
            if (q == null) {
                return null;
            }
            
            q = LINQUtils.ApplyOrdering(q, orderBy, orderDesc, orderFnsMap);
            if (postOrderFn != null) {
                q = postOrderFn.Invoke(q);
            }
            if (q == null) {
                return null;
            }

            //TODO: Revise this later
            IQueryable<TOut> q2 = (transformFn != null)
                ? q.Select(e => transformFn.Invoke(e))
                : ((typeof(T) == typeof(TOut))
                    ? (IQueryable<TOut>)q
                    : null);       
            if (q2 == null) {
                return null;
            }

            var item = q2.FirstOrDefault();
            if (postQueryFn != null) {
                var item2 = postQueryFn.Invoke(item);
                if (item2 != null) {
                    item = item2;
                }
            }

            return item;
        }

        public bool AddOrUpdateItem<T>(DbContext db, bool updateCond, T item, T item1 = null) where T : class {
            return (updateCond) ? UpdateItem(db, item, item1) : AddItem(db, item);
        }

        public bool AddItem<T>(DbContext db, T item) where T : class {
            db.Set<T>().Add(item);

            //TODO: Add options
             LINQUtils.FixUpdateRelatedEntityStates(db, item);

            return (db.SaveChanges() >= 1);
        }
        public bool UpdateItem<T>(DbContext db, T item, T item1) where T : class {
            var dbEntry1 = db.Entry(item1);
            dbEntry1.CurrentValues.SetValues(item);
            dbEntry1.State = EntityState.Modified;

            //TODO: Add options
            LINQUtils.FixUpdateRelatedEntityStates(db, item1);

            return (db.SaveChanges() >= 1);
        }

        public bool DeleteItem<T>(DbContext db, T item) where T : class {
            db.Set<T>().Remove(item);

            //TODO: Add options
            
            return (db.SaveChanges() >= 1);
        }
    }
}