using System;
using System.Collections.Generic;
using System.Linq;
using CustomerInquirySvc.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CustomerInquirySvc.Services.Impl
{
    public class CurrenciesServiceImpl : BaseServiceImpl, ICurrenciesService
    {
        public CurrenciesServiceImpl(IConfiguration appConfig, ILoggerFactory loggerFactory)
            : base(appConfig, loggerFactory)
        {
        }
        
        public IEnumerable<Currency> GetCurrencies(QueryOptions qOpts) {
            return GetCollection<Currency, object, Currency>(qOpts.Db,
                orderBy: qOpts.OrderBy, orderDesc: qOpts.OrderDesc ?? false
                /*
                ,
                postSelectFn: (q) => {
                    q = q.Include(e => e.Transactions)
                        .Include(e => e.Transactions.Select(tr => tr.Customer));

                    return q;
                }
                */
            );
        }
        public Currency GetCurrency(string code, QueryOptions qOpts) {
            return GetItem<Currency, string, Currency>(qOpts.Db, key: code,
                orderBy: qOpts.OrderBy, orderDesc: qOpts.OrderDesc ?? false,
                postSelectFn: (q) => q.Where(e => e.Code == code)
            );
        }

        public bool AddCurrency(Currency cust, QueryOptions qOpts) {
            throw new Exception("Not implemented yet");
        }
        
        public bool UpdateCurrency(Currency cust, QueryOptions qOpts) {   
            throw new Exception("Not implemented yet");
        }
        
        public bool DeleteCurrency(Currency cust, QueryOptions qOpts) {
            throw new Exception("Not implemented yet");
        }
    }
}