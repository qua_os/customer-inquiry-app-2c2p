using System;
using System.Collections.Generic;
using System.Linq;
using CustomerInquirySvc.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CustomerInquirySvc.Services.Impl
{
    public class CustomersServiceImpl : BaseServiceImpl, ICustomersService
    {
        public CustomersServiceImpl(IConfiguration appConfig, ILoggerFactory loggerFactory)
            : base(appConfig, loggerFactory)
        {
        }
        
        public IEnumerable<Customer> GetCustomers(CustomerQueryFilters filters, QueryOptions qOpts) {
            return GetCollection<Customer, CustomerQueryFilters, Customer>(qOpts.Db, filters: filters,
                orderBy: qOpts.OrderBy, orderDesc: qOpts.OrderDesc ?? false,
                postSelectFn: (q) => {
                    q = q.Include(e => e.Transactions)
                        .Include(e => e.Transactions.Select(tr => tr.Currency));
                        if (filters != null) {
                        if (!String.IsNullOrEmpty(filters.customerID)) {
                            q = q.Where(e => (e.CustomerId == filters.customerID));
                        }
                        if (!String.IsNullOrEmpty(filters.email)) {
                            q = q.Where(e => (e.ContactEmail == filters.email));
                        }
                    }

                    return q;
                }
            );
        }
        public IEnumerable<T> GetCustomersAs<T>(CustomerQueryFilters filters, Func<Customer, T> transformFn, QueryOptions qOpts)
            where T : class
        {
            return GetCollection<Customer, CustomerQueryFilters, T>(qOpts.Db, filters: filters,
                orderBy: qOpts.OrderBy, orderDesc: qOpts.OrderDesc ?? false,
                postSelectFn: (q) => {
                    q = q.Include(e => e.Transactions)
                        .Include(e => e.Transactions)
                            .ThenInclude(tr => tr.Currency);
                    if (filters != null) {
                        if (!String.IsNullOrEmpty(filters.customerID)) {
                            q = q.Where(e => (e.CustomerId == filters.customerID));
                        }
                        if (!String.IsNullOrEmpty(filters.email)) {
                            q = q.Where(e => (e.ContactEmail == filters.email));
                        }
                    }

                    return q;
                },
                transformFn: transformFn
            );
        }

        public Customer GetCustomer(string id, /*int? maxRecentTransactions,*/ QueryOptions qOpts) {
            return GetItem<Customer, string, Customer>(qOpts.Db, key: id,
                orderBy: qOpts.OrderBy, orderDesc: qOpts.OrderDesc ?? false,
                postSelectFn: (q) => q.Include(e => e.Transactions)
                    .Include(e => e.Transactions)
                        .ThenInclude(tr => tr.Currency)
                    .Where(e => e.CustomerId == id)
            );
        }
        public T GetCustomerAs<T>(string id, /*int? maxRecentTransactions,*/ Func<Customer, T> transformFn, QueryOptions qOpts)
            where T : class
        {
            return GetItem<Customer, string, T>(qOpts.Db, key: id,
                orderBy: qOpts.OrderBy, orderDesc: qOpts.OrderDesc ?? false,
                postSelectFn: (q) => q.Include(e => e.Transactions)
                    .Include(e => e.Transactions.Select(tr => tr.Currency))
                    .Where(e => e.CustomerId == id),
                transformFn: transformFn
            );
        }

        public bool AddCustomer(Customer cust, QueryOptions qOpts) {
            throw new Exception("Not implemented yet");
        }
        
        public bool UpdateCustomer(Customer cust, QueryOptions qOpts) {   
            throw new Exception("Not implemented yet");
        }
        
        public bool DeleteCustomer(Customer cust, QueryOptions qOpts) {
            throw new Exception("Not implemented yet");
        }
    }
}