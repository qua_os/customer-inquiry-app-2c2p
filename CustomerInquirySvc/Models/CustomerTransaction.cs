using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace CustomerInquirySvc.Models
{
    public class CustomerTransaction
    {
        public const string SUCCESS = "Success";
        public const string CANCELED = "Canceled";
        public const string FAILED = "Failed";

        [JsonIgnore]
        public long TransactionId { get; set; }
        
        [JsonProperty("TransactionId")]
        public string TransactionIdAsString {
            get { return TransactionId.ToString(); }
            set { TransactionId = Int64.Parse(value); }
        }

        [Required(AllowEmptyStrings = false)]
        [RegularExpression("^(\\d+)$")]
        [StringLength(10)]
        public string CustomerId { get; set; }
        public Customer Customer { get; set; }

        [Required]
        public DateTime TransactionDateTime { get; set; }
        
        [Required]
        public decimal Amount { get; set; }
        public double AmountAsDouble {
            get { return (double)Amount; }
            set { Amount = (decimal)value; }
        }

        [StringLength(5)]
        public string CurrencyCode { get; set; }
        public Currency Currency { get; set; }

        [StringLength(10)]
        public string Status { get; set; }
    }
}