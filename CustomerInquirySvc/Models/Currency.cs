using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomerInquirySvc.Models
{
    public class Currency
    {
        public Currency() {
            this.Transactions = new List<CustomerTransaction>();
        }

        public string Code { get; set; }

        public string  Name { get; set; }
        
        public string Description { get; set; }

        public ICollection<CustomerTransaction> Transactions { get; set;}
    }
}