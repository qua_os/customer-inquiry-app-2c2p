using System;
using System.ComponentModel.DataAnnotations;

namespace CustomerInquirySvc.Models
{
    public class CustomerTransactionQueryFilters
    {
        [StringLength(10)]
        [RegularExpression("^(\\d+)$")]
        public string CustomerId { get; set; }
        
        public DateTime? SinceDateTime { get; set; }

        public DateTime? BeforeDateTime { get; set; }

        public string Status { get; set; }
    }
}