using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace CustomerInquirySvc.Models
{
    public class CustomerInquiryDbContext : DbContext
    {
        protected ILoggerFactory LoggerFactory;

        public CustomerInquiryDbContext() : this(null, null) {
        }
        public CustomerInquiryDbContext(DbContextOptions options, ILoggerFactory loggerFactory)
            : base(options)
        {
            this.LoggerFactory = loggerFactory;
            if (this.LoggerFactory != null) {
                ILogger logger = LoggerFactory.CreateLogger<CustomerInquiryDbContext>();
                logger.LogInformation("DB Context Initialzied");
            }
        }
 
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerTransaction> CustomerTransactions { get; set; }
        public DbSet<Currency> Currencies { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (LoggerFactory != null) {
                ILogger logger = LoggerFactory.CreateLogger<CustomerInquiryDbContext>();
                logger.LogInformation("DB Context: OnConfiguring()");
            }
            optionsBuilder.UseLoggerFactory(LoggerFactory);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().ToTable("Customer")
                .HasKey(e => e.CustomerId);
            modelBuilder.Entity<Customer>()
                .HasIndex(e => e.ContactEmail).IsUnique();
            modelBuilder.Entity<Customer>().Property(e => e.CustomerId)
                .HasColumnName("customer_id")
                .HasColumnType("nvarchar")
                .HasMaxLength(10)
                .IsRequired();
            modelBuilder.Entity<Customer>().Property(e => e.CustomerName)
                .HasColumnName("customer_name")
                .HasColumnType("nvarchar")
                .HasMaxLength(30)
                .IsRequired();
            modelBuilder.Entity<Customer>().Property(e => e.ContactEmail)
                .HasColumnName("contact_email")
                .HasColumnType("nvarchar")
                .HasMaxLength(25)
                .IsRequired();
            modelBuilder.Entity<Customer>().Property(e => e.MobileNo)
                .HasColumnName("mobile_no")
                .HasColumnType("nvarchar")
                .HasMaxLength(10);

            modelBuilder.Entity<CustomerTransaction>().ToTable("Customer_Transaction")
                .HasKey(e => e.TransactionId);
            //modelBuilder.Entity<CustomerTransaction>().HasOne(e => e.Customer);
            modelBuilder.Entity<CustomerTransaction>()
                .HasOne(t => t.Customer)
                .WithMany(c => c.Transactions).HasForeignKey(t => t.CustomerId);
            modelBuilder.Entity<CustomerTransaction>()
                .HasOne(t => t.Currency)
                .WithMany(c => c.Transactions).HasForeignKey(t => t.CurrencyCode);
            modelBuilder.Entity<CustomerTransaction>().Property(e => e.TransactionId)
                .HasColumnName("id")
                .HasColumnType("bigint")
                .ValueGeneratedOnAdd()
                .UseSqlServerIdentityColumn();
            modelBuilder.Entity<CustomerTransaction>().Ignore(e => e.TransactionIdAsString);
            modelBuilder.Entity<CustomerTransaction>().Property(e => e.CustomerId)
                .HasColumnName("customer_id")
                .HasColumnType("nvarchar")
                .HasMaxLength(10)
                .IsRequired();
            modelBuilder.Entity<CustomerTransaction>().Property(e => e.TransactionDateTime)
                .HasColumnName("transaction_datetime")
                .HasColumnType("datetime")
                .HasDefaultValueSql("GetDate()")
                .IsRequired();
            modelBuilder.Entity<CustomerTransaction>().Property(e => e.Amount)
                .HasColumnName("amount")
                .HasColumnType("decimal(10, 2)")
                .HasMaxLength(10)
                .IsRequired();
            modelBuilder.Entity<CustomerTransaction>().Ignore(e => e.AmountAsDouble);
            modelBuilder.Entity<CustomerTransaction>().Property(e => e.CurrencyCode)
                .HasColumnName("currency_code")
                .HasColumnType("nvarchar")
                .HasMaxLength(5);
            modelBuilder.Entity<CustomerTransaction>().Property(e => e.Status)
                .HasColumnName("trans_status")
                .HasColumnType("nvarchar")
                .HasMaxLength(10);

            modelBuilder.Entity<Currency>().ToTable("Currency")
                .HasKey(e => e.Code);
            modelBuilder.Entity<Currency>().Property(e => e.Code).IsRequired()
                .HasColumnName("code")
                .HasColumnType("nvarchar")
                .HasMaxLength(5);
            modelBuilder.Entity<Currency>().Property(e => e.Name)
                .HasColumnName("name")
                .HasColumnType("nvarchar")
                .HasMaxLength(50);
            modelBuilder.Entity<Currency>().Property(e => e.Description)
                .HasColumnName("description")
                .HasColumnType("nvarchar")
                .HasMaxLength(255);
        }
    }
}