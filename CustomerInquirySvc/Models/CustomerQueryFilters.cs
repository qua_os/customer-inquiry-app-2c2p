using System.ComponentModel.DataAnnotations;

namespace CustomerInquirySvc.Models
{
    public class CustomerQueryFilters
    {
        [RegularExpression("^(\\d+)$")]
        [StringLength(10)]
        public string customerID { get; set; }

        [EmailAddress]
        [StringLength(25)]
        public string email { get; set; }

        public int? maxRecentTransactions { get; set; }
    }
}