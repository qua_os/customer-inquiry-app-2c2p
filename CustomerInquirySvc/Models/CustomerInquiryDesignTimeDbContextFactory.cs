using System;
using System.Diagnostics;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CustomerInquirySvc.Models
{
    public class CustomerInquiryDesignTimeDbContextFactory : IDesignTimeDbContextFactory<CustomerInquiryDbContext>
    {
        protected ILoggerFactory LoggerFactory;

        CustomerInquiryDesignTimeDbContextFactory(ILoggerFactory loggerFactory) {
            this.LoggerFactory = loggerFactory;
        }

        public CustomerInquiryDbContext CreateDbContext(string[] args)
        {
            if (!System.Diagnostics.Debugger.IsAttached) {
                System.Diagnostics.Debugger.Launch();
                while (!System.Diagnostics.Debugger.IsAttached) {
                    System.Threading.Thread.Sleep(100);
                }
            }
            //ILogger logger = Microsoft.Extensions.Logging.
            string cwd = Directory.GetCurrentDirectory();
            string confFile = "appsettings.Development.json";
            Debug.WriteLine("Current base path: {0}", (object)cwd);
            /*if (!File.Exists(Path.Join(cwd, confFile))) {
                throw new Exception(String.Format("Config File not found: {0} in dir: {1}", confFile, cwd));
            }*/
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(cwd)
                .AddJsonFile(confFile)
                .Build();
            var builder = new DbContextOptionsBuilder<CustomerInquiryDbContext>();
            var connectionString = configuration.GetConnectionString("CustomerInquiryDB");
            Debug.WriteLine("Got connection string: {0}", (object)connectionString);
            builder.UseSqlServer(connectionString);

            return new CustomerInquiryDbContext(builder.Options, LoggerFactory);
        }
    }
}