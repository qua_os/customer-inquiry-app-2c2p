using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace CustomerInquirySvc.Models {
    public class Customer {
        public Customer () {
            this.Transactions = new List<CustomerTransaction>();
        }

        [Required(AllowEmptyStrings = false)]
        [RegularExpression("^(\\d+)$")]
        [StringLength(10)]
        public string CustomerId { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(30)]
        public string CustomerName { get; set; }

        [EmailAddress]
        [Required(AllowEmptyStrings = false)]
        [StringLength(25)]
        public string ContactEmail { get; set; }

        [Phone]
        [StringLength(10)]
        public string MobileNo { get; set; }

        public ICollection<CustomerTransaction> Transactions { get; set; }

        public IEnumerable<CustomerTransaction> getRecentTransactions (int count) {
            return this.Transactions.OrderByDescending (e => e.TransactionDateTime).Take(count);
        }
    }
}