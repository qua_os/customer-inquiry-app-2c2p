using System.Collections.Generic;
using System.Security.Principal;

namespace CustomerInquirySvc.Models
{
    public class QueryOptions
    {
        public CustomerInquiryDbContext Db { get; set; }
        public IPrincipal User { get; set; }
        public IEnumerable<string> SelectedColumns { get; set; }
        public string OrderBy { get; set; }

        public bool? OrderDesc { get; set; }
    }
}