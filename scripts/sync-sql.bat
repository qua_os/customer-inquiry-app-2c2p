@echo off

SET APP_PATH=..
SET CLIENT_PATH=CustomerInquiryClient
SET SVC_PATH=CustomerInquirySvc

cd %APP_PATH%
dotnet ef migrations script --project CustomerInquirySvc

xcopy /S %SVC_PATH%\*.sql scripts\sql\
