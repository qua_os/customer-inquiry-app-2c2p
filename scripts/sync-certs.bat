@echo off

SET APP_PATH=..
SET CLIENT_PATH=%APP_PATH%\CustomerInquiryClient
SET SVC_PATH=%APP_PATH%\CustomerInquirySvc

openssl x509 -pubkey -outform der -in %SVC_PATH%\customer-inquiry-2c2p.cert.pem -out %CLIENT_PATH%\pubkeys\customer-inquiry-2c2p.crt
xcopy /S %CLIENT_PATH%\pubkeys\customer-inquiry-2c2p.crt %CLIENT_PATH%\src\assets\
