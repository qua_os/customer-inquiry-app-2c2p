@echo off

SET APP_PATH=..
SET CLIENT_PATH=%APP_PATH%\CustomerInquiryClient
SET SVC_PATH=%APP_PATH%\CustomerInquirySvc

xcopy /S %CLIENT_PATH%\dist\customer-inquiry-client.bundle.min.js %SVC_PATH%\wwwroot\assets\js\

