/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  TextInput,
  StatusBar,
  TouchableOpacity,
  Platform
} from 'react-native';


//TODO: Revise this (Workaround for NewAppScreen not found issue)
/*
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
*/

import debug from "debug";
import util from "util";

import styles from "./App-styles";
import appConfig from "./app.json";
import CustomerForm from "./components/customer-form";
import CustomerInquirySvcClientImpl_Fetch from "./services/impl/customer-inquiry-svc-client-impl-fetch";

const DEBUG_NS = "customer-inquiry-client-2c2p.App";

const clientID = Platform.OS;
const clientConfig = (clientID) ? appConfig.clients[clientID] : null;
debug(DEBUG_NS)(`Creating client for: ${clientID} with config: ${util.inspect(clientConfig)}`);
const client = new CustomerInquirySvcClientImpl_Fetch({
  baseURL: appConfig.service.url,
  clientID: clientID,
  clientToken: (clientConfig) ? clientConfig.token : null
});

const App = () => {
  return (
    <Fragment>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <CustomerForm client={client}></CustomerForm>
          </View>
        </ScrollView>
      </SafeAreaView>
    </Fragment>
  );
};


export default App;
