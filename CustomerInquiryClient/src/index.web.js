/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import appConfig from './app.json';
//import qwiz from "qwiz/src/index";

AppRegistry.registerComponent(appConfig.appName, () => App);
AppRegistry.runApplication(appConfig.appName, {
    initialProps: {},
    rootTag: document.getElementById('react-app')
});
