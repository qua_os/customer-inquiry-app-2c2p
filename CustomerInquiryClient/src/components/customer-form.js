
import React, {Fragment} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  FlatList
} from 'react-native';

import styles from "../App-styles";

class CustomerForm extends React.Component {
    constructor(props) {
        super(props);
        this.customerIdRef = React.createRef();
        this.emailRef = React.createRef();
        this.searchBtnRef = React.createRef();
        this.resultsRef = React.createRef();
        this.client = props.client;
        this.state = {
            loading: false,
            formInput: {
                customerID: null,
                email: null
            },
            customers: null,
            lastError: null
        };
    }

    renderCustomerInfo(item, idx) {
        return (
            <View key={idx} style={styles.listItem}>
              <Text style={styles.paragraph}>Customer ID: {item.customerID}</Text>
              <Text style={styles.paragraph}>Name: {item.name}</Text>
              <Text style={styles.paragraph}>Email: {item.email}</Text>
              <Text style={styles.paragraph}>Mobile: {item.mobile}</Text>
              <Text style={styles.paragraph}>Latest Transactions:</Text>
              <FlatList renderItem={this.renderCustomerTransaction}></FlatList>
            </View>
        );
    }

    renderCustomerTransaction(item, idx) {
        return (
            <Text style={styles.paragraph}>{item.TransactionDateTime} - {item.Amount} {item.CurrencyCode}</Text>
        );

    }

    render() {
        return (
            <View style={styles.customerForm}>
              <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>Customer Inquiry App</Text>
                <Text style={styles.sectionDescription}>
                    Please enter <Text style={styles.highlight}>Customer ID</Text>
                    or <Text style={styles.highlight}>Email</Text>
                </Text>
                <View style={styles.paragraph}>
                    <Text style={styles.label}>Customer ID: </Text>
                    <TextInput ref={this.customerIdRef} style={styles.textInput} onChangeText={this.onCustomerIdChanged.bind(this)}></TextInput>
                </View>
                <View style={styles.paragraph}>
                    <Text style={styles.label}>Email: </Text>
                    <TextInput ref={this.emailRef} style={styles.textInput} onChangeText={this.onEmailChanged.bind(this)}></TextInput>
                </View>
                <View style={styles.paragraph}>
                    <TouchableOpacity ref={this.emailRef} style={(this.state.loading) ? styles.disabledButton : styles.button} onPress={this.onSearchBtnPressed.bind(this)}>
                        <Text>Search</Text>
                    </TouchableOpacity>
                </View>
              </View>
              <View style={styles.sectionContainer}>
                <Text style={styles.error}>{this.state.lastError}</Text>
              </View>
              <View style={styles.sectionContainer} ref={this.resultsRef}>
                <FlatList renderItem={this.renderCustomerInfo}></FlatList>
              </View>
            </View>
        );
    }

    onCustomerIdChanged(text) {
        const formInput = { ...this.state.formInput };
        formInput.customerID = text;
        this.setState({ formInput: formInput });
    }
    onEmailChanged(text) {
        const formInput = { ...this.state.formInput };
        formInput.email = text;
        this.setState({ formInput: formInput });
    }

    onSearchBtnPressed(evt) {
        const view = this;
        if (this.state.loading) {
            return false;
        }

        view.setState({ loading: true });
        console.log("Querying for customer with filters: ", this.state.formInput);

        this.client.getCustomers(this.state.formInput)
            .then((data) => {
                if (data.errorMessage) {
                    return Promise.reject(new Error(`Error from service: ${data.errorMessage}`));
                }
                view.setState({ loading: false });
                let customers = (Array.isArray(data))
                    ? data
                    : [ data ];
                view.setState({ customers: customers });
            })
            .catch((err) => {
                view.setState({ loading: false });
                view.setState({ lastError: err.message });
            });
    }
}

export default CustomerForm;
