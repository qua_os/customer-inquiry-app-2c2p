
import { fetch } from "react-native-ssl-pinning";
import qwiz from "qwiz";
import CustomerInquiryService from "../customer-inquiry-svc";

class CustomerInquirySvcClientImpl_Fetch extends CustomerInquiryService {
    constructor(props) {
        super();
        this.baseURL = "";
        this.clientID = null;
        this.clientToken = null;
        (props) && qwiz.utils.merge(this, props, { deep: true });
    }

    getCustomers(filters) {
        const _static = CustomerInquirySvcClientImpl_Fetch;
        
        return this.callAPI("GET", _static.ENDPOINT_GET_CUSTS, filters);
    }

    getCustomer(id, maxTrans) {
        const _static = CustomerInquirySvcClientImpl_Fetch;
        
        return this.callAPI("GET", [ _static.ENDPOINT_GET_CUSTS, id ],
            {
                maxRecentTransactions: maxTrans
            }
        );
    }

    callAPI(method, url, reqData) {
        const client = this;

        let reqBody = null;
        if (method === "GET") {
            url = qwiz.utils.text.getURL(this.baseURL, url, reqData);
        } else {
            reqBody = reqData;
        }

        return fetch(url, {
            method: method ,
            //timeoutInterval: communication_timeout, // milliseconds
            body: reqBody,
            // your certificates array (needed only in android) ios will pick it automatically
            sslPinning: {
                certs: ["customer-inquiry-2c2p"]
            },
            headers: {
                Accept: "application/json; charset=utf-8",
                "Access-Control-Allow-Origin": "*",
                "X-App-Client-ID": client.clientID,
                "X-App-Client-Token": client.clientToken
            }
        })
            .then((response) => {
                console.log(`response received ${response}`);
                return response.json();
            })
            .catch((err) => {
                console.error(`error: ${err}`);
                return Promise.reject(err);
            });
    }
}
CustomerInquirySvcClientImpl_Fetch.ENDPOINT_GET_CUSTS = "/api/Customers";


export default CustomerInquirySvcClientImpl_Fetch;

