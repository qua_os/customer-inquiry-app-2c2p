
import { StyleSheet } from 'react-native';
//import { Colors } from 'react-native/Libraries/NewAppScreen';

//TODO: Revise this (Workaround for NewAppScreen not found issue)
const Colors = {
  white: "#FFFFFF",
  lighter: "#C0C0C0",
  gray: "#808080",
  dark: "#404040",
  black: "#000000",
  red: "#FF0000",
  green: "#00FF00",
  blue: "#0000FF"
};

const styles = StyleSheet.create({
    scrollView: {
      backgroundColor: Colors.lighter,
    },
    engine: {
      position: 'absolute',
      right: 0,
    },
    body: {
      backgroundColor: Colors.white,
    },
    sectionContainer: {
      marginTop: 32,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      fontSize: 24,
      fontWeight: '600',
      color: Colors.black,
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',
      color: Colors.dark,
    },
    highlight: {
      fontWeight: '700',
    },
    textInput: {
      display: "flex",
      borderWidth: 1,
      borderColor: Colors.lighter
    },
    button: {
      display: "flex",
      backgroundColor: Colors.lighter,
      color: Colors.black,
      borderWidth: 2,
      borderColor: Colors.dark,
      borderRadius: 25
    },
    disabledButton: {
      display: "flex",
      backgroundColor: Colors.lighter,
      color: Colors.dark,
      borderWidth: 2,
      borderColor: Colors.dark,
      borderRadius: 25
    },
    footer: {
      color: Colors.dark,
      fontSize: 12,
      fontWeight: '600',
      padding: 4,
      paddingRight: 12,
      textAlign: 'right',
    },
    error: {
      color: Colors.red
    }
  });

  export default styles;
