"use strict";

import qwiz from "qwiz";

class CustomerQueryFilters {
    constructor(props) {
        this.customerID = null;
        this.email = null;
        (props) && qwiz.utils.merge(this, props);
    }
}

export default CustomerQueryFilters;
