"use strict";

import qwiz from "qwiz";

class Customer {
    constructor(props) {
        this.customerID = null;
        this.email = null;
        this.mobile = null;
        this.transactions = [];
        (props) && qwiz.utils.merge(this, props, { deep: true });
    }
}

export default Customer;
