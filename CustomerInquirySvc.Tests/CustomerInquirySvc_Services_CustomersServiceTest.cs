using NUnit.Framework;
using CustomerInquirySvc.Services;
using CustomerInquirySvc.Services.Impl;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using CustomerInquirySvc.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.InMemory;
using System;

namespace CustomerInquirySvc.UnitTests.Services
{
    [TestFixture]
    [Category("Services.CustomersService")]
    public class CustomerInquirySvc_Services_CustomersServiceTest
    {
        protected CustomerInquiryDbContext DummyDbContext;
        protected CustomersServiceImpl CustomersService;

        [SetUp]
        public void Setup()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.Test.json")
                //.AddEnvironmentVariables()
                .Build();

            var serviceProvider = new ServiceCollection()
                .AddSingleton<IConfiguration>(config)
                .AddOptions()
                //.Configure<AppSettings>(options => options)
                .AddLogging(cfg => cfg.AddConsole())
                .Configure<LoggerFilterOptions>(cfg => cfg.MinLevel = LogLevel.Debug)
                .AddDbContext<CustomerInquiryDbContext>(options =>
                    options.UseInMemoryDatabase(Guid.NewGuid().ToString()))
                .AddSingleton<CustomersServiceImpl, CustomersServiceImpl>()
                .BuildServiceProvider();
                
            this.DummyDbContext = serviceProvider.GetService<CustomerInquiryDbContext>();
            this.CustomersService = serviceProvider.GetService<CustomersServiceImpl>();
        }

        [Test]
        [Order(0)]
        [Description("Service could be instantiated")]
        public void TestServiceInstantiation()
        {
            Assert.NotNull(CustomersService);
        }
        
        //TODO: System.InvalidOperationException : The entity type 'string' was not found. Ensure that the entity type has been added to the model.
        /*
        [Test]
        [Order(1)]
        [Description("Service could add and retrieve Customer data")]
        public void TestServiceAdd()
        {
            var customer = new Customer() {
                CustomerId = "123",
                CustomerName = "Dummy",
                ContactEmail = "test@localhost.com"
            };
            var qOpts = new QueryOptions() {
                Db = this.DummyDbContext
            };
            Assert.IsTrue(CustomersService.AddCustomer(customer, qOpts));
            var cust2 = CustomersService.GetCustomer(customer.CustomerId, qOpts);
            Assert.NotNull(cust2);
            Assert.Equals(customer, cust2);
        }
        */
    }
}